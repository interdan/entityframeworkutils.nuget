﻿namespace Interdan.SwaggerForEF.Swagger
{
    using Interdan.SwaggerForEF.MetaModels;
    using Microsoft.OpenApi.Any;
    using Microsoft.OpenApi.Models;
    using Newtonsoft.Json;
    using System;
    using System.Collections.Generic;

    internal delegate IOpenApiAny GetExampleValueDelegate(ColumnInfo columnInfo);

    public class ExampleValueBuilders
    {
        private static readonly Dictionary<Type, GetExampleValueDelegate> formatters = new Dictionary<Type, GetExampleValueDelegate>()
        {
            { typeof(string), StringExampleBuilder },
            { typeof(int?), NullExampleBuilder },
            { typeof(decimal), DecimalExampleBuilder },
            { typeof(DateTime?), NullableDatetimeExampleBuilder },
        };

        private static IOpenApiAny NullExampleBuilder(ColumnInfo columnInfo) => new OpenApiNull();

        private static IOpenApiAny DecimalExampleBuilder(ColumnInfo columnInfo)
        {
            decimal example = 0;
            decimal scale = 1;
            for (int decimalPosition = 1; decimalPosition <= columnInfo.DecimalPointNumberForDecimalType; decimalPosition++)
            {
                scale /= 10;
                example += scale * decimalPosition;
            }

            example = columnInfo.IntegerPointNumberForDecimalType + example;

            return new OpenApiDouble((double)example);
        }

        private static IOpenApiAny NullableDatetimeExampleBuilder(ColumnInfo columnInfo)
        {
            if (columnInfo.IsRequired) return null;

            var datetime = DateTime.Now.ToString("yyyy-MM-ddTHH:mm:ssZ");
            return new OpenApiString($"null or {datetime}");
        }

        private static IOpenApiAny StringExampleBuilder(ColumnInfo columnInfo)
        {
            if (columnInfo.IsRequired || columnInfo.MaxLength > 0)
            {
                string maxLength = columnInfo.MaxLength == 0 ? "∞" : columnInfo.MaxLength.ToString();
                return new OpenApiString($"{(columnInfo.IsRequired ? "Required " : string.Empty)}string, {maxLength} chars");
            }

            return new OpenApiNull();
        }

        internal static void CheckValueExample(ColumnInfo columnInfo, OpenApiSchema value)
        {
            if (columnInfo == null) return;

            GetExampleValueDelegate valueResolver;
            if (!formatters.TryGetValue(columnInfo.ClrType, out valueResolver)) return;

            var exampleValue = valueResolver(columnInfo);
            if (exampleValue == null) return;

            value.Example = exampleValue;
        }
    }
}
