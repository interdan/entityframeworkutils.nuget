﻿namespace Interdan.SwaggerForEF.Swagger
{
    using AutoMapper;
    using Interdan.SwaggerForEF.EntityFramework;
    using Interdan.SwaggerForEF.MetaModels;
    using Microsoft.OpenApi.Models;
    using Swashbuckle.AspNetCore.SwaggerGen;
    using System;
    using System.Linq;

    public class SchemaFilter : ISchemaFilter
    {
        private readonly IMapper mapper;
        private readonly IEntitiesMetaProvider entitiesMetaProvider;

        public SchemaFilter(IMapper mapper, IEntitiesMetaProvider entitiesMetaProvider)
        {
            this.mapper = mapper;
            this.entitiesMetaProvider = entitiesMetaProvider;
        }

        private ColumnInfo GetInfo(Type modelType, string fieldName)
        {
            TypeMap typeMap = mapper.ConfigurationProvider.GetAllTypeMaps().Where(_ => _.SourceType == modelType).FirstOrDefault();
            if (typeMap == null) return null;

            var sourceType = typeMap.Types.DestinationType;

            foreach (PropertyMap propertyMap in typeMap.PropertyMaps)
            {
                if (propertyMap.Ignored || propertyMap.SourceMember == null) continue;

                if (propertyMap.SourceMember.Name.Equals(fieldName, StringComparison.OrdinalIgnoreCase))
                {
                    TableInfo tableInfo;
                    if (entitiesMetaProvider.EntitiesMeta.TryGetValue(sourceType, out tableInfo))
                    {
                        ColumnInfo columnInfo;
                        tableInfo.TryGetValue(propertyMap.DestinationName, out columnInfo);
                        return columnInfo;
                    }
                }
            }

            return null;
        }

        public void Apply(OpenApiSchema schema, SchemaFilterContext context)
        {
            if (schema.Properties == null || schema.Properties.Count == 0) return;

            var items = schema.Properties.ToList();

            foreach (var (key, value) in items)
            {
                if (value != null)
                {
                    ExampleValueBuilders.CheckValueExample(GetInfo(context.Type, key), value);
                }
            }
        }
    }
}
