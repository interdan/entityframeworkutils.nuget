﻿namespace Microsoft.Extensions.DependencyInjection
{
    using Interdan.SwaggerForEF.EntityFramework;
    using Interdan.SwaggerForEF.Swagger;
    using Microsoft.EntityFrameworkCore;
    using Swashbuckle.AspNetCore.SwaggerGen;
    using System;

    public static class SwaggerConfigurationExtension
    {
        public static IServiceCollection AddSwaggerGenWithEFMetaExamples<TDbContext>(
            this IServiceCollection services,
            Action<SwaggerGenOptions> setupAction = null)
            where TDbContext : DbContext
        {
            services.AddSingleton<IEntitiesMetaProvider, EntitiesMetaProvider<TDbContext>>();
            services.AddSwaggerGen(c =>
            {
                c.SchemaFilter<SchemaFilter>();

                setupAction?.Invoke(c);
            });

            return services;
        }
    }
}
