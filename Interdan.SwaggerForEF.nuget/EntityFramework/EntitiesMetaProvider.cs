﻿namespace Interdan.SwaggerForEF.EntityFramework
{
    using Interdan.SwaggerForEF.MetaModels;
    using Microsoft.EntityFrameworkCore;
    using Microsoft.EntityFrameworkCore.Metadata;
    using Microsoft.Extensions.DependencyInjection;
    using System;
    using System.Collections.Generic;

    public class EntitiesMetaProvider<TDbContext> : IEntitiesMetaProvider
        where TDbContext : DbContext
    {
        private readonly IServiceProvider serviceProvider;
        private readonly Dictionary<Type, BuildMetadataDelegate> metaBuilders;

        private delegate void BuildMetadataDelegate(IProperty propertyType, ColumnInfo columntInfo);

        private object locker = new object();
        private volatile EntitiesMeta entitiesMeta;

        public EntitiesMeta EntitiesMeta
        {
            get
            {
                if (entitiesMeta == null)
                {
                    lock (locker)
                    {
                        if (entitiesMeta == null)
                        {
                            entitiesMeta = ReadEntitiesMeta();
                        }
                    }
                }

                return entitiesMeta;
            }
        }

        private EntitiesMeta ReadEntitiesMeta()
        {
            using (var serviceScope = serviceProvider.CreateScope())
            {
                DbContext dbContext = serviceScope.ServiceProvider.GetRequiredService<TDbContext>();
                return BuildEntitiesMeta(dbContext);
            }
        }

        private EntitiesMeta BuildEntitiesMeta(DbContext dbContext)
        {
            var entitiesMeta = new EntitiesMeta();

            foreach (IEntityType entityType in dbContext.Model.GetEntityTypes())
            {
                entitiesMeta.Add(entityType.ClrType, BuildTableInfo(entityType));
            }

            return entitiesMeta;
        }

        private TableInfo BuildTableInfo(IEntityType entityType)
        {
            var tableInfo = new TableInfo();

            foreach (var propertyType in entityType.GetProperties())
            {
                tableInfo.Add(propertyType.Relational().ColumnName, BuildColumnInfo(propertyType));
            }

            return tableInfo;
        }

        private void BuildStringMetadata(IProperty propertyType, ColumnInfo columntInfo)
        {
            columntInfo.MaxLength = propertyType.GetMaxLength() ?? 0;
            columntInfo.IsRequired = !propertyType.IsNullable;
        }

        private void BuildDecimalMetadata(IProperty propertyType, ColumnInfo columntInfo)
        {
            var decimalTypeMetaParts = propertyType.Relational().ColumnType
                     .Replace("decimal", string.Empty, StringComparison.OrdinalIgnoreCase)
                     .Replace('(', ' ')
                     .Replace(')', ' ')
                     .Trim()
                     .Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);

            columntInfo.DecimalPointNumberForDecimalType = decimalTypeMetaParts.Length < 2 ? 0 : Convert.ToInt32(decimalTypeMetaParts[1].Trim());

            columntInfo.IntegerPointNumberForDecimalType =
                decimalTypeMetaParts.Length < 1 ? 0 : Convert.ToInt32(decimalTypeMetaParts[0].Trim())
                - columntInfo.DecimalPointNumberForDecimalType;
        }

        private ColumnInfo BuildColumnInfo(IProperty propertyType)
        {
            var columntInfo = new ColumnInfo() { ClrType = propertyType.ClrType };

            BuildMetadataDelegate metaBuilder;
            if (metaBuilders.TryGetValue(propertyType.ClrType, out metaBuilder))
            {
                metaBuilder(propertyType, columntInfo);
            }

            return columntInfo;
        }

        public EntitiesMetaProvider(IServiceProvider serviceProvider)
        {
            this.serviceProvider = serviceProvider;
            metaBuilders = new Dictionary<Type, BuildMetadataDelegate>()
            {
                { typeof(string), BuildStringMetadata },
                { typeof(decimal), BuildDecimalMetadata },
            };
        }
    }
}
