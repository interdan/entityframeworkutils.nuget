﻿namespace Interdan.SwaggerForEF.EntityFramework
{
    using Interdan.SwaggerForEF.MetaModels;

    public interface IEntitiesMetaProvider
    {
        EntitiesMeta EntitiesMeta { get; }
    }
}
