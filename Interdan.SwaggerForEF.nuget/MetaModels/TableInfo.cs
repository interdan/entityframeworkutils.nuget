﻿namespace Interdan.SwaggerForEF.MetaModels
{
    using System;
    using System.Collections.Generic;

    public class TableInfo : Dictionary<string, ColumnInfo>
    {
        public TableInfo()
            : base(StringComparer.OrdinalIgnoreCase) { }
    }


}
