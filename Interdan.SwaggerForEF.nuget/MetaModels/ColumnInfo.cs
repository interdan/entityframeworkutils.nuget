﻿namespace Interdan.SwaggerForEF.MetaModels
{
    using System;

    public class ColumnInfo
    {
        public int MaxLength { get; set; }

        public int DecimalPointNumberForDecimalType { get; set; }

        public int IntegerPointNumberForDecimalType { get; set; }

        public bool IsRequired { get; set; }

        public Type ClrType { get; internal set; }
    }
}
