﻿namespace Interdan.EntityFrameworkUtils.Entities
{
    using System;

    public interface ICreatedTrackedEntity
    {
        DateTime Created { get; set; }
    }
}
