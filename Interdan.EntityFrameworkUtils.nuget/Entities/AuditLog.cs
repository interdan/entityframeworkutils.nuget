namespace Interdan.EntityFrameworkUtils.Entities
{
    using System;
    using System.ComponentModel.DataAnnotations;

    public class AuditLog
    {
        public const int UserIdentifierLength = 100;


        public int Id { get; set; }

        public string TableName { get; set; }

        public DateTime DateTime { get; set; }

        [MaxLength(UserIdentifierLength)]
        public string UserIdentifier { get; set; }

        public string KeyValues { get; set; }

        public string OldValues { get; set; }

        public string NewValues { get; set; }
    }
}
