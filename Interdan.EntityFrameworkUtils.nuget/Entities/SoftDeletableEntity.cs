﻿namespace Interdan.EntityFrameworkUtils.Entities
{
    using System;

    public abstract class SoftDeletableEntity : ICreatedTrackedEntity
    {
        public int Id { get; set; }

        public DateTime Created { get; set; }

        public DateTime? Deleted { get; set; }
    }
}
