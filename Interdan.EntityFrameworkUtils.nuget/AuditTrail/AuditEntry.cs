namespace Interdan.EntityFrameworkUtils.AuditTrail
{
    using Interdan.EntityFrameworkUtils.Entities;
    using Microsoft.EntityFrameworkCore.ChangeTracking;
    using Newtonsoft.Json;
    using System;
    using System.Collections.Generic;
    using System.Linq;

    public class AuditEntry
    {
        private readonly string userId;

        public AuditEntry(EntityEntry entry, string userId)
        {
            Entry = entry;
            this.userId = userId;
        }

        public EntityEntry Entry { get; }

        public string TableName { get; set; }

        public Dictionary<string, object> KeyValues { get; } = new Dictionary<string, object>();

        public Dictionary<string, object> OldValues { get; } = new Dictionary<string, object>();

        public Dictionary<string, object> NewValues { get; } = new Dictionary<string, object>();

        public List<PropertyEntry> TemporaryProperties { get; } = new List<PropertyEntry>();

        public bool HasTemporaryProperties => TemporaryProperties.Any();

        public bool HasChanges => CheckIsThereAnyChange();

        private bool CheckIsThereAnyChange()
        {
            if (NewValues.Count != OldValues.Count) return true;

            foreach (var (fieldName, newValue) in NewValues)
            {
                object oldValue;
                if (!OldValues.TryGetValue(fieldName, out oldValue)) return true;

                if (!object.Equals(oldValue, newValue)) return true;
            }

            return false;
        }

        public AuditLog ToAudit()
        {
            var audit = new AuditLog();

            audit.TableName = TableName;
            audit.DateTime = DateTime.UtcNow;
            audit.KeyValues = JsonConvert.SerializeObject(KeyValues);
            audit.OldValues = OldValues.Count == 0 ? null : JsonConvert.SerializeObject(OldValues);
            audit.NewValues = NewValues.Count == 0 ? null : JsonConvert.SerializeObject(NewValues);
            audit.UserIdentifier = userId;

            return audit;
        }
    }
}
