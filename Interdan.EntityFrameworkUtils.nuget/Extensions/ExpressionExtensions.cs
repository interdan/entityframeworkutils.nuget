﻿namespace System.Linq.Expressions
{
    using Microsoft.EntityFrameworkCore;
    using Remotion.Linq.Parsing.ExpressionVisitors;
    using System.Collections.Generic;
    using System.Reflection;

    public static class ExpressionExtensions
    {
        public static string GetPropertyName(this Expression propertyLambda)
        {
            LambdaExpression lambdaExpression = propertyLambda as LambdaExpression;
            if (lambdaExpression == null) return string.Empty;

            MemberExpression body = lambdaExpression.Body as MemberExpression;

            if (body == null)
            {
                UnaryExpression ubody = (UnaryExpression)lambdaExpression.Body;
                body = ubody.Operand as MemberExpression;
            }

            return body.Member.Name;
        }


        private static volatile MethodInfo likeMethod;

        private static MethodInfo GetLikeMethod()
        {
            if (likeMethod == null)
            {
                likeMethod = typeof(DbFunctionsExtensions).GetMethods()
                               .Where(method => method.Name.Equals("Like") && method.GetParameters().Length == 3).FirstOrDefault();
            }

            return likeMethod;
        }

        /// <summary>
        /// Build Entity Framework "Where" filter using sql "Like" function and search predicate
        /// </summary>
        /// <typeparam name="TEntity">DB entity class</typeparam>
        /// <param name="query">source sequence</param>
        /// <param name="properties">string property names of DB entity where to search</param>
        /// <param name="searchPredicate">string used with like operator, like "%{term}%" or "{term}%" etc.</param>
        /// <returns>filtered IQueryable<TEntity></returns>
        public static IQueryable<TEntity> BuildStringSearchFilter<TEntity>(
            this IQueryable<TEntity> query,
            IEnumerable<string> properties,
            string searchPredicate)
            where TEntity : class
        {
            if (properties.Count() == 0) return query;

            var entityParameter = Expression.Parameter(typeof(TEntity), "e");
            var orElseExpressions = new Stack<Expression>();

            foreach (var propertyName in properties)
            {
                var stringPropExpression = Expression.Property(entityParameter, propertyName);

                var likeExpression = Expression.Call(
                    GetLikeMethod(),
                    Expression.Constant(EF.Functions),
                    stringPropExpression,
                    Expression.Constant(searchPredicate));

                orElseExpressions.Push(likeExpression);
            }

            Expression body = orElseExpressions.CombineExpressions(Expression.OrElse);
            var filter = Expression.Lambda<Func<TEntity, bool>>(body, entityParameter);

            return query.Where(filter);
        }

        public delegate Expression TwoArgumentsExpressionDelegate(Expression left, Expression right);

        public static Expression CombineExpressions(
            this Stack<Expression> expressionsToCombine,
            TwoArgumentsExpressionDelegate twoArgsExpression)
        {
            if (expressionsToCombine.Count == 0)
            {
                throw new ArgumentException("orElse expressions list can't be empty!");
            }

            if (expressionsToCombine.Count == 1) return expressionsToCombine.First();

            return twoArgsExpression(
                expressionsToCombine.Pop(),
                expressionsToCombine.CombineExpressions(twoArgsExpression));
        }

        public static Expression ReplaceSingleParameter(this LambdaExpression lambda, ParameterExpression newParameter) =>
          ReplacingExpressionVisitor.Replace(lambda.Parameters.Single(), newParameter, lambda.Body);
    }
}
