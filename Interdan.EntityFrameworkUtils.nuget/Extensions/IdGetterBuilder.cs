﻿namespace Interdan.EntityFrameworkUtils.Extensions
{
    using System;
    using System.Collections.Concurrent;
    using System.Linq.Expressions;
    using System.Reflection;

    public static class IdGetterBuilder
    {
        private const string SINGLE_COLUMN_PK_DEFAULT_NAME = "Id";
        private static readonly ConcurrentDictionary<Type, Func<object, int>> cachedIdGetters = new ConcurrentDictionary<Type, Func<object, int>>();

        public static MemberExpression IdProperty<T>(this ParameterExpression parameter) =>
            IdProperty(parameter, typeof(T), false);

        private static MemberExpression IdProperty(this ParameterExpression parameter, Type itemType, bool castToType)
        {
            PropertyInfo entityIdProperty = itemType.GetProperty(SINGLE_COLUMN_PK_DEFAULT_NAME);
            if (entityIdProperty == null)
            {
                return null;
            }

            Expression entityTypeParameter = castToType ? Expression.Convert(parameter, itemType) : (Expression)parameter;
            var resolvePropertyExpression = Expression.Property(entityTypeParameter, entityIdProperty);

            return resolvePropertyExpression;
        }

        public static Func<object, int> BuildGetIdMethod(this Type entityType)
        {
            var entityObjectTypeParameter = Expression.Parameter(typeof(object), "entity");

            var idProperty = entityObjectTypeParameter.IdProperty(entityType, true);
            if (idProperty == null) return null;

            var lambda = Expression.Lambda<Func<object, int>>(idProperty, entityObjectTypeParameter);
            return lambda.Compile();
        }

        public static int ResolveIdValue<T>(this T item)
        {
            Type type = typeof(T) == typeof(object) ? item.GetType() : typeof(T);

            Func<object, int> idGetter;
            if (!cachedIdGetters.TryGetValue(type, out idGetter))
            {
                idGetter = BuildGetIdMethod(type);
                cachedIdGetters.TryAdd(type, idGetter);
            }

            return idGetter(item);
        }
    }
}
