﻿namespace Interdan.EntityFrameworkUtils.Extensions
{
    public static class PrimaryKeyValueResolver
    {
        public static int ResolvePrimaryKeyValue(this object entity)
        {
            var idGetter = entity.GetType().BuildGetIdMethod();

            if (idGetter != null)
            {
                return idGetter(entity);
            }

            return 0;
        }
    }
}
