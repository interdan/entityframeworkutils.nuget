﻿namespace System
{
    using System.Collections.Concurrent;
    using System.Collections.Generic;
    using System.Linq.Expressions;
    using System.Reflection;

    public static class TypeExtensions
    {
        private static readonly ConcurrentDictionary<Type, HashSet<string>> PublicPropertiesResultsCache = new ConcurrentDictionary<Type, HashSet<string>>();

        public static HashSet<string> GetPublicProperties(this Type type)
        {
            HashSet<string> result;

            if (!PublicPropertiesResultsCache.TryGetValue(type, out result))
            {
                result = new HashSet<string>(StringComparer.OrdinalIgnoreCase);
                foreach (var propertyInfo in type.GetProperties(BindingFlags.Public | BindingFlags.Instance))
                {
                    result.Add(propertyInfo.Name);
                }

                PublicPropertiesResultsCache.TryAdd(type, result);
            }

            return result;
        }

        public static Expression<Func<TEntity, bool>> BuildFilterExpression<TEntity>(this List<PropertyInfo> filterBy, object constantValue, Type constantType)
        {
            var (parameter, body) = BuildLambdaArguments(typeof(TEntity), filterBy, constantValue, constantType);
            return Expression.Lambda<Func<TEntity, bool>>(body, parameter);
        }

        public static LambdaExpression BuildFilterExpression(this PropertyInfo filterBy, Type entityType, object constantValue, Type constantType)
        {
            var (parameter, comparison) = BuildLambdaArguments(entityType, new List<PropertyInfo>() { filterBy }, constantValue, constantType);
            return Expression.Lambda(comparison, parameter);
        }

        private static (ParameterExpression, BinaryExpression) BuildLambdaArguments(Type entityType, List<PropertyInfo> filterBy, object constantValue, Type constantType)
        {
            ParameterExpression parameter = Expression.Parameter(entityType, "entity");

            Expression resolveTargetPropertyExpression = resolveTargetProperty(parameter, filterBy, 0);

            BinaryExpression comparison = Expression.Equal(
                resolveTargetPropertyExpression,
                Expression.Constant(constantValue, constantType));

            return (parameter, comparison);
        }

        private static Expression resolveTargetProperty(Expression currentExpression, List<PropertyInfo> filterBy, int pathIndex)
        {
            var nextExpression = Expression.Property(currentExpression, filterBy[pathIndex]);
            return pathIndex == (filterBy.Count - 1)
                ? nextExpression
                : resolveTargetProperty(nextExpression, filterBy, pathIndex + 1);
        }
    }
}
