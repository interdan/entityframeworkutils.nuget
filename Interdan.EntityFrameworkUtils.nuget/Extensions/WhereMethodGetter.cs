﻿namespace Interdan.EntityFrameworkUtils.Extensions
{
    using System;
    using System.Linq;
    using System.Reflection;

    public static class WhereMethodGetter
    {
        public static MethodInfo BuildWhereMethod(this Type entityType)
        {
            var methodsSet = typeof(Queryable).GetMethods();
            const string methodName = nameof(Queryable.Where);

            foreach (var method in methodsSet)
            {
                if (method.Name != methodName)
                {
                    continue;
                }

                var parameters = method.GetParameters();
                if (parameters.Length != 2)
                {
                    continue;
                }

                var predicateParameter = parameters[1].ParameterType;
                if (predicateParameter.GenericTypeArguments.Length != 1)
                {
                    continue;
                }

                var funcOfLambdaExpression = predicateParameter.GenericTypeArguments[0];
                if (funcOfLambdaExpression.GenericTypeArguments.Length == 2)
                {
                    return method.MakeGenericMethod(entityType);
                }
            }

            return null;
        }
    }
}
