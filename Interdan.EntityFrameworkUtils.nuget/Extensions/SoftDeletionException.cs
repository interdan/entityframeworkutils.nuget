﻿namespace Interdan.EntityFrameworkUtils.Extensions
{
    using System;

    public class SoftDeletionException : Exception
    {
        public SoftDeletionException(string message)
            : base(message) { }
    }
}
