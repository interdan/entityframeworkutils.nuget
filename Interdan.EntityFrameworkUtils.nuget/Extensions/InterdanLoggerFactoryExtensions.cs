﻿namespace Microsoft.Extensions.Logging
{
    public static class InterdanLoggerFactoryExtensions
    {
        public static ILogger CreateLogger<TEntity>(this ILoggerFactory logger, object loggerHolder, string suffix = "")
        {
            var entityName = typeof(TEntity).Name;
            var namespaceName = loggerHolder.GetType().Namespace;

            return logger
                .CreateLogger($"{namespaceName}.{entityName}{suffix}");
        }

        public static ILogger CreateLogger(this ILoggerFactory logger, object loggerHolder, string suffix = "")
        {
            var thisType = loggerHolder.GetType();
            var entityName = thisType.Name;
            var namespaceName = thisType.Namespace;

            return logger
                .CreateLogger($"{namespaceName}.{entityName}{suffix}");
        }
    }
}
