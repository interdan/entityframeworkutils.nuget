﻿namespace Microsoft.EntityFrameworkCore.Metadata.Builders
{
    using System;
    using System.Linq.Expressions;

    public static class EFExtensions
    {
        private static EntityTypeBuilder addQueryFilter(EntityTypeBuilder entityTypeBuilder, LambdaExpression expression)
        {
            if (expression == null) return entityTypeBuilder;

            LambdaExpression currentQueryFilter = entityTypeBuilder.Metadata.QueryFilter;
            if (currentQueryFilter == null)
            {
                entityTypeBuilder.HasQueryFilter(expression);
                return entityTypeBuilder;
            }

            var parameterType = Expression.Parameter(entityTypeBuilder.Metadata.ClrType, expression.Parameters[0].Name);
            var expressionFilter = expression.ReplaceSingleParameter(parameterType);

            var currentExpressionFilter = currentQueryFilter.ReplaceSingleParameter(parameterType);

            var finalLambda = Expression.Lambda(
                Expression.AndAlso(currentExpressionFilter, expressionFilter),
                parameterType);

            entityTypeBuilder.HasQueryFilter(finalLambda);

            return entityTypeBuilder;
        }

        public static IndexBuilder HasUniqueIndex<TEntity>(
            this EntityTypeBuilder<TEntity> entityTypeBuilder,
            Expression<Func<TEntity, object>> indexExpression,
            string sqlFilter = null)
            where TEntity : class =>
            entityTypeBuilder.HasIndex(indexExpression).HasFilter(sqlFilter).IsUnique();

        public static EntityTypeBuilder AddQueryFilter<T>(this EntityTypeBuilder entityTypeBuilder, Expression<Func<T, bool>> expression)
            where T : class =>
            addQueryFilter(entityTypeBuilder, expression);

        public static EntityTypeBuilder AddQueryFilter(this EntityTypeBuilder entityTypeBuilder, LambdaExpression expression) =>
            addQueryFilter(entityTypeBuilder, expression);
    }
}
