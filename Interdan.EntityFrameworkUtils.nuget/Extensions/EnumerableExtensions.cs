﻿namespace System.Collections.Generic
{
    using System;

    public static class EnumerableExtensions
    {
        public static void ForEach<T>(this IEnumerable<T> list, Action<T> action)
        {
            if (list == null) return;

            foreach (T item in list)
            {
                action(item);
            }
        }
    }
}
