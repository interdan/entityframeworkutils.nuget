﻿namespace Microsoft.EntityFrameworkCore
{
    using Interdan.EntityFrameworkUtils.DbExceptionHelpers;
    using Interdan.EntityFrameworkUtils.Enums;

    public static class DbUpdateExceptionExtension
    {
        public static ChangesSavingErrorTypes ResolveChangesSavingError(
            this DbUpdateException dbUpdateException)
        {
            var helper = HelperFactory.ResolveHelper(dbUpdateException.InnerException);

            return helper == null
                ? ChangesSavingErrorTypes.Unknown
                : helper.ResolveErrorType();
        }
    }
}
