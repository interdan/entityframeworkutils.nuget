﻿namespace Interdan.EntityFrameworkUtils.Extensions
{
    using Interdan.EntityFrameworkUtils.Entities;
    using Microsoft.EntityFrameworkCore;
    using Microsoft.EntityFrameworkCore.Metadata.Builders;
    using System;
    using System.Linq;
    using System.Linq.Expressions;
    using System.Reflection;

    public static class ModelBuilderExtensions
    {
        public static ModelBuilder SetDefaultOnDeleteBehavior(this ModelBuilder modelBuilder, DeleteBehavior behaviorToSet)
        {
            var cascadeFKs = modelBuilder.Model.GetEntityTypes()
                .SelectMany(t => t.GetForeignKeys())
                .Where(fk => !fk.IsOwnership && fk.DeleteBehavior == DeleteBehavior.Cascade);

            foreach (var foreignKey in cascadeFKs)
            {
                foreignKey.DeleteBehavior = behaviorToSet;
            }

            return modelBuilder;
        }

        public static ModelBuilder IgnoreSoftDeletedEntities(this ModelBuilder modelBuilder)
        {
            var softDetetableType = from entityType in modelBuilder.Model.GetEntityTypes()
                                    where typeof(SoftDeletableEntity).IsAssignableFrom(entityType.ClrType)
                                    select entityType;

            foreach (var entity in softDetetableType)
            {
                var clrType = entity.ClrType;
                modelBuilder
                    .Entity(clrType).AddQueryFilter(clrType.BuildFilterByDeleteProperty());
            }

            return modelBuilder;
        }

        private static LambdaExpression BuildFilterByDeleteProperty(this Type entityType)
        {
            PropertyInfo filterBy = entityType.GetProperty(nameof(SoftDeletableEntity.Deleted));
            return filterBy.BuildFilterExpression(entityType, null, filterBy.PropertyType);
        }
    }
}
