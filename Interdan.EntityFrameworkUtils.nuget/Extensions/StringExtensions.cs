﻿namespace System
{
    using System.Globalization;
    using System.Text;

    public static class StringExtensions
    {
        public static string ToCamelCase(this string value)
        {
            if (string.IsNullOrEmpty(value)) return string.Empty;

            return char.ToLowerInvariant(value[0]) + value.Substring(1);
        }

        public static string RemoveDiacritics(this string inputString)
        {
            string strFormD = inputString.Normalize(System.Text.NormalizationForm.FormD);

            var result = new StringBuilder();

            for (int ich = 0; ich < strFormD.Length; ich++)
            {
                UnicodeCategory unicodeCategory = CharUnicodeInfo.GetUnicodeCategory(strFormD[ich]);
                if (unicodeCategory != UnicodeCategory.NonSpacingMark)
                {
                    result.Append(strFormD[ich]);
                }
            }

            var resultString = result.ToString().Normalize(NormalizationForm.FormC);
            return resultString;
        }
    }
}
