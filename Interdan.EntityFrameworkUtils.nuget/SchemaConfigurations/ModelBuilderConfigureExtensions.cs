﻿namespace Microsoft.EntityFrameworkCore
{
    using Interdan.EntityFrameworkUtils.SchemaConfigurations;
    using System.Linq;

    public static class ModelBuilderConfigureExtensions
    {
        public static ModelBuilder ConfigureEntity<TEntity, TConfiguration>(this ModelBuilder modelBuilder)
           where TEntity : class
           where TConfiguration : IEntityConfiguration<TEntity>, new()
        {
            new TConfiguration().Configure(modelBuilder.Entity<TEntity>());

            return modelBuilder;
        }
    }
}
