﻿namespace Interdan.EntityFrameworkUtils
{
    public interface IUserIdentifierProvider
    {
        string UserIdentifier { get; }
    }
}
