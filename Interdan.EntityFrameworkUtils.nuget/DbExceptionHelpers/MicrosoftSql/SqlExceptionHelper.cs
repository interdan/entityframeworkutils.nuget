﻿namespace Interdan.EntityFrameworkUtils.DbExceptionHelpers.MicrosoftSql
{
    using Interdan.EntityFrameworkUtils.Enums;
    using System;
    using System.Data.SqlClient;

    public class SqlExceptionHelper : IDbExceptionHelper
    {
        private SqlException exception;

        public SqlExceptionHelper(SqlException exception)
        {
            this.exception = exception;
        }

        public ChangesSavingErrorTypes ResolveErrorType()
        {
            if (!Enum.IsDefined(typeof(SqlErrorTypes), exception.Number))
            {
                return ChangesSavingErrorTypes.Unknown;
            }


            switch ((SqlErrorTypes)exception.Number)
            {
                case SqlErrorTypes.UniqueIndexViolation:
                case SqlErrorTypes.UniqueConstraintViolation:
                    return ChangesSavingErrorTypes.UniqueIndexViolation;

                case SqlErrorTypes.StatementConflictedWithConstraint:
                    return ChangesSavingErrorTypes.AttemptToViolateDatabaseConstraint;

                case SqlErrorTypes.CannotInsertExplicitValueForIdentityColumn:
                    return ChangesSavingErrorTypes.CannotInsertExplicitValueForIdentityColumn;

                default:
                    return ChangesSavingErrorTypes.Unknown;
            }
        }
    }
}
