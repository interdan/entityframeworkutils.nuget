﻿namespace Interdan.EntityFrameworkUtils.DbExceptionHelpers.MicrosoftSql
{
    public enum SqlErrorTypes
    {
        CannotInsertExplicitValueForIdentityColumn = 544,

        StatementConflictedWithConstraint = 547,

        UniqueIndexViolation = 2601,

        UniqueConstraintViolation = 2627,
    }
}
