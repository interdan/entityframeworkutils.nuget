﻿namespace Interdan.EntityFrameworkUtils.DbExceptionHelpers
{
    using Interdan.EntityFrameworkUtils.DbExceptionHelpers.MicrosoftSql;
    using System;
    using System.Data.SqlClient;

    internal class HelperFactory
    {
        public static IDbExceptionHelper ResolveHelper(Exception ex)
        {
            switch (ex)
            {
                case SqlException exception:
                    return new SqlExceptionHelper(exception);

                default:
                    return null;
            }
        }
    }
}
