﻿namespace Interdan.EntityFrameworkUtils.DbExceptionHelpers
{
    using Interdan.EntityFrameworkUtils.Enums;

    internal interface IDbExceptionHelper
    {
        ChangesSavingErrorTypes ResolveErrorType();
    }
}
