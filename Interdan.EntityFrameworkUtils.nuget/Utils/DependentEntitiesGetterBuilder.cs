﻿namespace Interdan.EntityFrameworkUtils.Utils
{
    using Interdan.EntityFrameworkUtils.Extensions;
    using Microsoft.EntityFrameworkCore;
    using System;
    using System.Collections.Concurrent;
    using System.Linq.Expressions;
    using System.Reflection;
    using System.Threading;
    using System.Threading.Tasks;

    public delegate Task ReadDependentEntitiesDelegate(DbContext dbContext, int id);

    public static class DependentEntitiesGetterBuilder
    {
        private static readonly ConcurrentDictionary<(Type, string), ReadDependentEntitiesDelegate> DependentEntitiesGetters = new ConcurrentDictionary<(Type, string), ReadDependentEntitiesDelegate>();

        public static ReadDependentEntitiesDelegate ResolveDependentEntitiesGetter(this Type entityType, string foreignKeyPropertyName)
        {
            ReadDependentEntitiesDelegate result;

            (Type, string) key = (entityType, foreignKeyPropertyName);
            if (!DependentEntitiesGetters.TryGetValue(key, out result))
            {
                result = BuildDependentEntitiesGetter(entityType, foreignKeyPropertyName);
                DependentEntitiesGetters.TryAdd(key, result);
            }

            return result;
        }


        /// <summary>
        /// Builds the function like:
        ///     (DbContext dbContext, int id) =>
        ///         dbContext.Set<TSource>().Where<TSource>(entity => entity.BrandId == id).ToListAsync<TSource>()
        /// </summary>
        /// <param name="entityType">EF entity type</param>
        /// <param name="foreignKeyPropertyName">int FK property name on dependent entity</param>
        /// <returns>Compiled, ready to use method</returns>
        private static ReadDependentEntitiesDelegate BuildDependentEntitiesGetter(Type entityType, string foreignKeyPropertyName)
        {
            var dbContextParameter = Expression.Parameter(typeof(DbContext), "dbContext");
            var idParameter = Expression.Parameter(typeof(int), "id");

            var getSetMethod = typeof(DbContext).GetMethod("Set", 1, new Type[0]).MakeGenericMethod(entityType);
            var getDbSetExpression = Expression.Call(dbContextParameter, getSetMethod);

            #region inner expression

            var entityParameter = Expression.Parameter(entityType, "entity");
            PropertyInfo entityIdProperty = entityType.GetProperty(foreignKeyPropertyName);
            if (entityIdProperty == null)
            {
                return null;
            }

            var innerExpressionBody = Expression.Equal(
                Expression.Property(entityParameter, entityIdProperty),
                Expression.Convert(idParameter, entityIdProperty.PropertyType));

            var innerExpression = Expression.Lambda(innerExpressionBody, entityParameter);

            #endregion

            var whereMethod = entityType.BuildWhereMethod();
            var whereResults = Expression.Call(whereMethod, getDbSetExpression, innerExpression);

            var toListAsyncMethod = typeof(EntityFrameworkQueryableExtensions)
                .GetMethod("ToListAsync").MakeGenericMethod(entityType);

            var cancellationTokenParameter = Expression.Constant(default(CancellationToken));

            var wholeBody = Expression.Call(toListAsyncMethod, whereResults, cancellationTokenParameter);

            var castedResultToVoidTask = Expression.Convert(wholeBody, typeof(Task));

            var resultExpression = Expression.Lambda<ReadDependentEntitiesDelegate>(castedResultToVoidTask, dbContextParameter, idParameter);

            return resultExpression.Compile();
        }
    }
}
