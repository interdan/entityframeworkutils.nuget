﻿namespace Interdan.EntityFrameworkUtils.Utils
{
    using System;
    using System.Threading;
    using System.Threading.Tasks;

    // https://www.ryadel.com/en/asyncutil-c-helper-class-async-method-sync-result-wait/
    public static class AsyncUtil
    {
        private static readonly TaskFactory TaskFactory = new
            TaskFactory(CancellationToken.None, TaskCreationOptions.None, TaskContinuationOptions.None, TaskScheduler.Default);

        /// <returns>Result of the passed task</returns>
        /// <summary>
        /// Executes an async Task<T> method which has a T return type synchronously
        /// USAGE: T result = AsyncUtil.RunSync(() => AsyncMethod<T>());
        /// </summary>
        /// <typeparam name="TResult">Return Type</typeparam>
        /// <param name="task">Task<T> method to execute</param>
        public static TResult RunSync<TResult>(Func<Task<TResult>> task)
        {
            return TaskFactory
                           .StartNew(task)
                           .Unwrap()
                           .GetAwaiter()
                           .GetResult();
        }
    }
}
