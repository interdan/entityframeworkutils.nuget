﻿namespace Interdan.EntityFrameworkUtils.Utils
{
    using Interdan.EntityFrameworkUtils.Entities;
    using Interdan.EntityFrameworkUtils.Extensions;
    using Microsoft.EntityFrameworkCore;
    using Microsoft.EntityFrameworkCore.ChangeTracking;
    using Microsoft.EntityFrameworkCore.Internal;
    using Microsoft.EntityFrameworkCore.Metadata.Internal;
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;

    public static class DependenciesGetter
    {
        public static async Task<List<EntityEntry>> GetDependencies(this DbContext dbContext, object entity, bool topLevelOnly = false)
        {
            return await GetDependencies(dbContext, new List<object>() { entity }, topLevelOnly);
        }

        public static async Task<List<EntityEntry>> GetDependencies(this DbContext dbContext, IEnumerable entitiesToProcess, bool topLevelOnly = false)
        {
            var dependencies = new List<EntityEntry>();

            var processedItems = new HashSet<(Type, int)>();
            foreach (var entity in entitiesToProcess)
            {
                await TraverseRecursively(entity, dbContext, processedItems, dependencies, topLevelOnly);
            }

            return dependencies;
        }

        private static async Task TraverseRecursively(
            object entity,
            DbContext dbContext,
            HashSet<(Type, int)> processedItems,
            List<EntityEntry> dependencies,
            bool topLevelOnly)
        {
            int id = entity.ResolvePrimaryKeyValue();
            if (!processedItems.Add((entity.GetType(), id)))
            {
                return;
            }

            var isHardDeletable = !typeof(SoftDeletableEntity).IsAssignableFrom(entity.GetType());
            var meta = dbContext.Entry(entity).Metadata;
            var foreignKeys = meta.GetReferencingForeignKeys();

            foreach (ForeignKey fk in foreignKeys)
            {
                bool dontNeedToChangeDefaultBehavior = isHardDeletable
                    && (fk.DeleteBehavior == DeleteBehavior.SetNull || fk.DeleteBehavior == DeleteBehavior.Restrict);
                if (dontNeedToChangeDefaultBehavior) continue;

                bool throwErrorIfAnyEntityFound = !isHardDeletable
                    && fk.DeleteBehavior == DeleteBehavior.Restrict;

                var foreignKeyPropertyName = fk.Properties.FirstOrDefault().Name;
                Type entityType = fk.DeclaringEntityType.ClrType;

                // with version 3.0 of EF core, it should be possible to use the next, much simple approach
                // var tableName = dbContext.Model.FindEntityType(entityType).Relational().TableName;
                // IEnumerable dependentEntitiesCollection = await dbContext.Set(entityType).FromSql($"select * from [{tableName}] where [{foreignKeyPropertyName}] = @id", new SqlParameter("id", id)).ToListAsync();
                var findDependentEntitiesTaskGetter = entityType.ResolveDependentEntitiesGetter(foreignKeyPropertyName);
                Task findDependentEntitiesTask = findDependentEntitiesTaskGetter(dbContext, id);
                await findDependentEntitiesTask;
                var dependentEntitiesCollection = ((dynamic)findDependentEntitiesTask).Result as IEnumerable;

                foreach (object entityForSoftDeletion in dependentEntitiesCollection)
                {
                    EntityEntry entityEntryForSoftDeletion = dbContext.Entry(entityForSoftDeletion);

                    if (throwErrorIfAnyEntityFound)
                    {

                        throw new SoftDeletionException(
                            $@"Existing ""{entityEntryForSoftDeletion.Metadata.ClrType.Name}"" entity depends on ""{meta.ClrType.Name}"" entity that expected to be deleted!");
                    }

                    dependencies.Add(entityEntryForSoftDeletion);

                    if (!topLevelOnly)
                    {
                        await TraverseRecursively(entityForSoftDeletion, dbContext, processedItems, dependencies, topLevelOnly);
                    }
                }
            }
        }
    }
}
