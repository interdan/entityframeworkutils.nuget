﻿namespace Interdan.EntityFrameworkUtils
{
    using Interdan.EntityFrameworkUtils.Entities;
    using Microsoft.EntityFrameworkCore;
    using System.Linq;

    public static class DefaultValuesDefinition
    {
        public static ModelBuilder DefineDefaultValues(this ModelBuilder modelBuilder)
        {
            DefineDefaultValuesForSoftDeletableEntity(modelBuilder);

            return modelBuilder;
        }

        private static void DefineDefaultValuesForSoftDeletableEntity(ModelBuilder modelBuilder)
        {
            var entityTypes = modelBuilder.Model.GetEntityTypes()
               .Where(t => typeof(ICreatedTrackedEntity).IsAssignableFrom(t.ClrType));

            foreach (var entityType in entityTypes)
            {
                modelBuilder.Entity(
                    entityType.Name,
                    x => x.Property(nameof(ICreatedTrackedEntity.Created)).IsRequired().HasDefaultValueSql("GetUtcDate()"));
            }
        }
    }
}
