﻿namespace Interdan.EntityFrameworkUtils
{
    using Interdan.EntityFrameworkUtils.AuditTrail;
    using Interdan.EntityFrameworkUtils.Entities;
    using Interdan.EntityFrameworkUtils.Extensions;
    using Interdan.EntityFrameworkUtils.Utils;
    using Microsoft.EntityFrameworkCore;
    using Microsoft.EntityFrameworkCore.ChangeTracking;
    using Microsoft.EntityFrameworkCore.ChangeTracking.Internal;
    using Newtonsoft.Json;
    using System;
    using System.Collections;
    using System.Collections.Concurrent;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Threading;
    using System.Threading.Tasks;

    public class AuditTrailDbContext : DbContext
    {
        private static ConcurrentDictionary<Type, HashSet<PropertyInfo>> ignoredProperties = new ConcurrentDictionary<Type, HashSet<PropertyInfo>>();

        private readonly IUserIdentifierProvider userIdentifierProvider;

        private IEnumerable DeletedEntities => (from x in ChangeTracker.Entries()
                                                where x.State == EntityState.Deleted
                                                select x.Entity).ToList();

        public DbSet<AuditLog> Audits { get; set; }

        public AuditTrailDbContext(DbContextOptions options, IUserIdentifierProvider userIdentifierProvider)
        : base(options)
        {
            this.userIdentifierProvider = userIdentifierProvider;
        }

        public sealed override int SaveChanges()
        {
            var dependencies = AsyncUtil.RunSync(() => this.GetDependencies(DeletedEntities));

            OnChangesSaving(dependencies);

            var auditEntries = OnBeforeSaveChanges();

            var result = SaveChangesWithoutAuditTrail();

            OnAfterSaveChanges(auditEntries).GetAwaiter().GetResult();

            return result;
        }

        public sealed override async Task<int> SaveChangesAsync(CancellationToken cancellationToken = default)
        {
            HandleDeletedEntitiesMarkedAsModified();
            var all = (from x in ChangeTracker.Entries()
                       where x.Metadata.ClrType.Name == "CarDimensions"
                       select x).ToList();

            OnChangesSaving(await this.GetDependencies(DeletedEntities));

            var auditEntries = OnBeforeSaveChanges();

            var result = await SaveChangesAsyncWithoutAuditTrail(cancellationToken);

            await OnAfterSaveChanges(auditEntries);

            return result;
        }

        private void HandleDeletedEntitiesMarkedAsModified()
        {
            var entries = ChangeTracker.Entries().ToList();

            var entitiesToHandleConceptualNull = new List<InternalEntityEntry>();
            foreach (EntityEntry entry in entries.Where(e =>
                e.State == EntityState.Modified || e.State == EntityState.Added))
            {
                InternalEntityEntry internalEntry = GetInternalEntityEntry(entry);
                if (internalEntry.HasConceptualNull)
                {
                    entitiesToHandleConceptualNull.Add(internalEntry);
                }
            }

            entitiesToHandleConceptualNull.ForEach(x => x.HandleConceptualNulls(false));
        }

        private InternalEntityEntry GetInternalEntityEntry(EntityEntry entityEntry)
        {
            var internalEntry = (InternalEntityEntry)entityEntry
                .GetType()
                .GetProperty("InternalEntry", BindingFlags.NonPublic | BindingFlags.Instance)
                .GetValue(entityEntry);

            return internalEntry;
        }

        protected sealed override void OnModelCreating(ModelBuilder modelBuilder)
        {
            OnModelCreatingWithAuditTrail(modelBuilder);

            modelBuilder
                .IgnoreSoftDeletedEntities()
                .DefineDefaultValues();
        }

        protected virtual void OnModelCreatingWithAuditTrail(ModelBuilder modelBuilder) { }

        public virtual Task<int> SaveChangesAsyncWithoutAuditTrail(CancellationToken cancellationToken = default) => base.SaveChangesAsync(cancellationToken);

        public virtual int SaveChangesWithoutAuditTrail() => base.SaveChanges();

        private List<AuditEntry> OnBeforeSaveChanges()
        {
            ChangeTracker.DetectChanges();
            var auditEntries = new List<AuditEntry>();
            foreach (var entry in ChangeTracker.Entries())
            {
                if (entry.Entity is AuditLog || entry.State == EntityState.Detached || entry.State == EntityState.Unchanged)
                {
                    continue;
                }

                var currentValues = entry.GetDatabaseValues();
                if (currentValues != null)
                {
                    entry.OriginalValues.SetValues(currentValues);
                }

                var auditEntry = new AuditEntry(entry, userIdentifierProvider.UserIdentifier);
                auditEntry.TableName = entry.Metadata.Relational().TableName;
                auditEntries.Add(auditEntry);

                HashSet<PropertyInfo> ignoredProperties;
                if (!AuditTrailDbContext.ignoredProperties.TryGetValue(entry.Entity.GetType(), out ignoredProperties))
                {
                    ignoredProperties = ResolveIgnoredProperties(entry.Properties);
                    AuditTrailDbContext.ignoredProperties.TryAdd(entry.Entity.GetType(), ignoredProperties);
                }

                foreach (var property in entry.Properties)
                {
                    if (ignoredProperties.Contains(property.Metadata.PropertyInfo))
                    {
                        continue;
                    }

                    if (property.IsTemporary)
                    {
                        // value will be generated by the database, get the value after saving
                        auditEntry.TemporaryProperties.Add(property);
                        continue;
                    }

                    string propertyName = property.Metadata.Name;
                    if (property.Metadata.IsPrimaryKey())
                    {
                        auditEntry.KeyValues[propertyName] = property.CurrentValue;
                        continue;
                    }

                    switch (entry.State)
                    {
                        case EntityState.Added:
                            auditEntry.NewValues[propertyName] = property.CurrentValue;
                            break;

                        case EntityState.Deleted:
                            auditEntry.OldValues[propertyName] = property.OriginalValue;
                            break;

                        case EntityState.Modified:
                            if (property.IsModified)
                            {
                                auditEntry.OldValues[propertyName] = property.OriginalValue;
                                auditEntry.NewValues[propertyName] = property.CurrentValue;
                            }

                            break;
                    }
                }
            }

            // Save audit entities that have all the modifications
            foreach (var auditEntry in auditEntries.Where(_ => !_.HasTemporaryProperties))
            {
                if (auditEntry.HasChanges)
                {
                    Audits.Add(auditEntry.ToAudit());
                }
            }

            // keep a list of entries where the value of some properties are unknown at this step
            return auditEntries.Where(_ => _.HasTemporaryProperties).ToList();
        }

        private HashSet<PropertyInfo> ResolveIgnoredProperties(IEnumerable<PropertyEntry> properties)
        {
            var result = new HashSet<PropertyInfo>();

            foreach (var property in properties)
            {
                var ignoreAttributes = property
                    .Metadata.PropertyInfo.GetCustomAttributes(typeof(JsonIgnoreAttribute), true);

                if (ignoreAttributes.Length > 0)
                {
                    result.Add(property.Metadata.PropertyInfo);
                }
            }

            return result;
        }

        private void OnChangesSaving(List<EntityEntry> dependencies)
        {
            dependencies.ForEach(PerformDeletionOnEntity);

            var entities = ChangeTracker.Entries()
                .Where(x =>
                    (x.State == EntityState.Added || x.State == EntityState.Deleted)
                    && x.Entity is ICreatedTrackedEntity).ToList();

            var utcNow = DateTime.UtcNow;

            foreach (var entityEntry in entities)
            {
                var entity = entityEntry.Entity as ICreatedTrackedEntity;

                if (entityEntry.State == EntityState.Added)
                {
                    // need to specify this on EF level to make it available for audit trail
                    entity.Created = utcNow;
                }
                else if (entityEntry.State == EntityState.Deleted
                    && entityEntry.Entity is SoftDeletableEntity)
                {
                    PerformDeletionOnEntity(entityEntry);
                }
            }
        }

        private void PerformDeletionOnEntity(EntityEntry entityEntry)
        {
            var baseEntity = entityEntry.Entity as SoftDeletableEntity;
            if (baseEntity != null)
            {
                baseEntity.Deleted = DateTime.UtcNow;
                entityEntry.State = EntityState.Modified;
            }
            else
            {
                entityEntry.State = EntityState.Deleted;
            }
        }

        private Task OnAfterSaveChanges(List<AuditEntry> auditEntries)
        {
            if (auditEntries == null || auditEntries.Count == 0)
            {
                return Task.CompletedTask;
            }

            foreach (var auditEntry in auditEntries)
            {
                // Get the final value of the temporary properties
                foreach (var prop in auditEntry.TemporaryProperties)
                {
                    if (prop.Metadata.IsPrimaryKey())
                    {
                        auditEntry.KeyValues[prop.Metadata.Name] = prop.CurrentValue;
                    }
                    else
                    {
                        auditEntry.NewValues[prop.Metadata.Name] = prop.CurrentValue;
                    }
                }

                if (auditEntry.HasChanges)
                {
                    Audits.Add(auditEntry.ToAudit());
                }
            }

            return SaveChangesAsync();
        }
    }
}
