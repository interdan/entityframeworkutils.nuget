﻿namespace Interdan.EntityFrameworkUtils.Enums
{
    public enum ChangesSavingErrorTypes
    {
        None,

        Unknown,

        UniqueIndexViolation,

        AttemptToViolateDatabaseConstraint,

        HasDependentEntities,

        CannotInsertExplicitValueForIdentityColumn,
    }
}
