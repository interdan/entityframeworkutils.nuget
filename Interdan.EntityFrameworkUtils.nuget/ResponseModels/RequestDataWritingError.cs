﻿namespace Interdan.EntityFrameworkUtils.ResponseModels
{
    using Interdan.EntityFrameworkUtils.Enums;

    public class RequestDataWritingError
    {
        public RequestDataWritingError(ChangesSavingErrorTypes error, object errorDetails = null)
        {
            Message = error.ToString();
            ErrorCode = (int)error;
            ErrorDetails = errorDetails;
        }

        public string Message { get; }

        public int ErrorCode { get; }

        public object ErrorDetails { get; }
    }
}
