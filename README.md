# EntityFrameworkUtils

Library provides soft deletion and audit trail for your .Net EF Core 2.2 application

The library supports the nexts features:

* soft deletion for your real entities (entities with int "Id" property, that used as Primary key)
* audit trail for all the entities in your application (that have tables in database)

## Installation
    dotnet add package Interdan.EntityFrameworkUtils --version 1.0.0

## Basic using

Inherit your DbContext from `AuditTrailDbContext` instead of `DbContext`:

```csharp
public class AuditTrailDbContext : DbContext
```

Inherit the entities you want to support soft deletion feature from `SoftDeletableEntity` class

```csharp
public class MyEntity : SoftDeletableEntity
```

Implement `IUserIdentifierProvider` interface and inject it. For example:

```csharp

public class Auth0UserIdentifierProvider : IUserIdentifierProvider
{
  private readonly IHttpContextAccessor httpContextAccessor;

  private string userIdentifier;

  public Auth0UserIdentifierProvider(IHttpContextAccessor httpContextAccessor)
  {
      this.httpContextAccessor = httpContextAccessor;
  }

  public string UserIdentifier => userIdentifier ?? (userIdentifier = resolveUserId());

  private string resolveUserId()
  {
      ClaimsPrincipal user = httpContextAccessor?.HttpContext?.User;
      if (user == null) return string.Empty;

      const string EMAIL_CLAIM_TYPE = "http://email";

      var email = user.Claims
          .Where(claim => claim.Type == EMAIL_CLAIM_TYPE)
          .ToList().FirstOrDefault()?.Value;

      return email ?? string.Empty;
  }
}

...
// in Startup.cs file
public void ConfigureServices(IServiceCollection services)
{
  services.AddTransient<IUserIdentifierProvider, Auth0UserIdentifierProvider>();
  ...
}
```

All the entities will have a change history in the Audit table. `OldValues` and `NewValues` columns contain the JSON data for entity state before and after changes respectively. If you don't need to track some property (or it's hard to serialize) you can ignore it with the standard `[JsonIgnore]` attribute.

All the entities that are inherited from `SoftDeletableEntity` with that have Int32 `Id` property (that is Primary key) and they won't be deleted from the database, but rather the new nullable datetime2 `Deleted` property will store the datetime of this "deletion".

As well the `Created` field will be added to your entity. The field will store insertion DateTime.